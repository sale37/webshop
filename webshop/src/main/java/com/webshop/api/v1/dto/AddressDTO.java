package com.webshop.api.v1.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String country;
    private String shippingAddress;
    private String address2;
    private String city;
    private String State;
    private String zip;

    //private Set<UserDTO> users;
}
