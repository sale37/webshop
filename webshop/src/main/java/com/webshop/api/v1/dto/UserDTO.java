package com.webshop.api.v1.dto;

import com.webshop.models.UserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Long id;
    private UserType userType;
    private String username;
    private String password;
    private String dateOfBirth;
    private String dateOfRegistration;
    private String email;

    private Set<AddressDTO> addresses;
}
