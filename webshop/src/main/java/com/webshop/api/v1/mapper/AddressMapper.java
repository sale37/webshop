package com.webshop.api.v1.mapper;

import com.webshop.api.v1.dto.AddressDTO;
import com.webshop.models.Address;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AddressMapper {

    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    AddressDTO addressToAddressDTO(Address address);

    Address addressDTOtoAddress(AddressDTO addressDTO);
}
