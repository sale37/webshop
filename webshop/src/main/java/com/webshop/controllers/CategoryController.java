package com.webshop.controllers;

import com.webshop.models.Category;
import com.webshop.services.CategoryService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(CategoryController.BASE_URL)
@RequiredArgsConstructor
@Slf4j
public class CategoryController {

    public static final String BASE_URL = "/api/v1/categories";

    private final CategoryService categoryService;

    @ApiOperation(value = "Add new category")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Category addCategory(@RequestBody String name){
        log.info("Received request for adding category with name: " + name);
        return categoryService.addCategory(name);
    }

    @ApiOperation(value = "View all categories")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Category> findAll(){
        log.info("Received request finding all categories");
        return categoryService.findAll();
    }

    @ApiOperation(value = "Find category by ID")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Category findById(@PathVariable Long id){
        log.info("Received request for finding category with id: " + id);
        return categoryService.findById(id);
    }

    @ApiOperation(value = "Add product to category")
    @PostMapping("/{categoryId}/product/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public Category addProduct(@PathVariable("categoryId") Long categoryId, @PathVariable("productId") Long productId){
        log.info("Received request for adding product with id: " + productId + " into the category with id: " + categoryId);
        return categoryService.addProduct(categoryId, productId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id){
        log.info("Received request for deleting category with id: " + id);
        categoryService.deleteById(id);
    }
}
