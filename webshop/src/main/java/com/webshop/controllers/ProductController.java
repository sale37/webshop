package com.webshop.controllers;

import com.webshop.models.Product;
import com.webshop.services.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductController.BASE_URL)
@RequiredArgsConstructor
@Slf4j
public class ProductController {

    public static final String BASE_URL = "/api/v1/products";

    private final ProductService productService;

    @ApiOperation(value = "View all products")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Product> findAll(){
        log.info("Received request for finding all products");
        return productService.findAll();
    }

    @ApiOperation(value = "Find product by ID")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Product findById(@PathVariable Long id){
        log.info("Received request for finding product with id: " + id);
        return productService.findById(id);
    }

    @ApiOperation(value = "Delete product")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        log.info("Received request for deleting product with id: " + id);
        productService.deleteById(id);
    }
}
