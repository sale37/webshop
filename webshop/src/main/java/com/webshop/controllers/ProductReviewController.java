package com.webshop.controllers;

import com.webshop.models.ProductReview;
import com.webshop.services.ProductReviewService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductReviewController.BASE_URL)
@RequiredArgsConstructor
@Slf4j
public class ProductReviewController {

    public static final String BASE_URL = "/api/v1/reviews";

    private final ProductReviewService productReviewService;

    @PostMapping("/product/{productId}")
    @ResponseStatus(HttpStatus.CREATED)
    public ProductReview addReview(@PathVariable Long productId, @RequestBody ProductReview productReview) {
        log.info("Received request for adding review to the product with id: " + productId);
        return productReviewService.addReview(productId, productReview);
    }

    @GetMapping("/product/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductReview> showAllReviewsForProduct(@PathVariable Long productId){
        log.info("Received request to show all reviews for product with id: " + productId);
        return productReviewService.showAllReviewsForProduct(productId);
    }

    @GetMapping("/product/{productId}/review/{reviewId}")
    @ResponseStatus(HttpStatus.OK)
    public ProductReview showReview(@PathVariable Long productId, @PathVariable Long reviewId){
        log.info("Received request to show review with id: " + reviewId + " for product with id: " + productId);
        return productReviewService.showReview(productId, reviewId);
    }
}
