package com.webshop.controllers;

import com.webshop.exceptions.BadRequestException;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.exceptions.UnauthorizedException;
import com.webshop.exceptions.UnprocessableEntityException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@Slf4j
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleNotFoundException(ResourceNotFoundException exception, WebRequest request){
        log.error("Resource not found", exception);
        return new ResponseEntity<>(new ErrorResponse("Resource not found", exception.getMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({UnauthorizedException.class})
    public ResponseEntity<ErrorResponse> handleUnauthorizedException(UnauthorizedException ecxeption, WebRequest request){

        return new ResponseEntity<>(new ErrorResponse("Unauthorized", ecxeption.getMessage()), new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({UnprocessableEntityException.class})
    public ResponseEntity<ErrorResponse> handleunprocessableEntityException(UnprocessableEntityException exception, WebRequest request){

        return new ResponseEntity<>(new ErrorResponse("Unprocessable Entity", exception.getMessage()), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<ErrorResponse> handleBadRequestException(BadRequestException exception, WebRequest request){
        log.error("Bad request", exception);
        return new ResponseEntity<>(new ErrorResponse("Bad Request", exception.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @AllArgsConstructor
    @Getter
    class ErrorResponse{
        private String status;
        private String reason;
    }
}
