package com.webshop.controllers;

import com.webshop.models.ShoppingCart;
import com.webshop.services.ShoppingCartService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ShoppingCartController.BASE_URL)
@RequiredArgsConstructor
@Slf4j
public class ShoppingCartController {

    public static final String BASE_URL = "/api/v1/shoppingCart";

    private final ShoppingCartService shoppingCartService;

    @ApiOperation(value = "View all shopping carts")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ShoppingCart> findAll(){
        log.info("Received request for finding all shopping carts");
        return shoppingCartService.findAll();
    }

    @ApiOperation(value = "Create new shopping cart")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ShoppingCart createCart(){
        log.info("Received request for creating new shopping cart");
        return shoppingCartService.createCart();
    }

    @ApiOperation(value = "Show shopping cart by ID")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ShoppingCart showCart(@PathVariable Long id){
        log.info("Received request for finding shopping cart with id: " + id);
        return shoppingCartService.showCart(id);
    }

    @ApiOperation(value = "Add product to shopping cart")
    @PostMapping("/{cartId}/product/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public ShoppingCart addItem(@PathVariable("cartId") Long cartId, @PathVariable("productId") Long productId){
        log.info("Received request for adding product with id: " + productId + " into the shopping cart with id: " + cartId);
        return shoppingCartService.addItem(cartId, productId);
    }

    @PostMapping("/{cartId}/checkout/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void checkout(@PathVariable("cartId") Long cartId, @PathVariable("userId") Long userId){
     shoppingCartService.checkout(cartId, userId);
    }
}
