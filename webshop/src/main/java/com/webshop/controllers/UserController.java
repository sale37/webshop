package com.webshop.controllers;

import com.webshop.models.User;
import com.webshop.services.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(UserController.BASE_URL)
@RequiredArgsConstructor
@Slf4j
public class UserController {

    public static final String BASE_URL = "/api/v1/users";

    private final UserService userService;

    @ApiOperation(value = "View all users")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<User> findAll() {
        log.info("Received request for finding all users");
        return (List<User>) userService.findAll();
    }

    @ApiOperation(value = "Find use by ID")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public User findById(@PathVariable Long id) {
        log.info("Received request for finding user with id:" + id);
        return userService.findById(id);
    }

    @ApiOperation(value = "Add new user")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User addUser(@RequestBody User user) {
        log.info("Received request for adding new user with id: " + user.getId());
        return userService.save(user);
    }

    @ApiOperation(value = "Update an existing user")
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public User updateUser(@RequestBody User user) {
        log.info("Received request for updating user with id: " + user.getId());
        return userService.update(user);
    }

    @ApiOperation(value = "Find user by username")
    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public User findUserByUsername(@RequestParam(value = "username") String username) {
        log.info("Received request for finding user with username: " + username);
        return userService.findByUsername(username);
    }

    @ApiOperation(value = "Delete user")
    @DeleteMapping()
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@RequestBody User user) {
        log.info("Received request for deleting user with id: " + user.getId());
        userService.delete(user);
    }
}
