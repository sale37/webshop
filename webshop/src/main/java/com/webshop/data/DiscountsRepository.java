package com.webshop.data;

import com.webshop.models.Discount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscountsRepository extends CrudRepository<Discount, Long> {

}
