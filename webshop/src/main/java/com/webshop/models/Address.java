package com.webshop.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "COUNTRY", nullable = false)
    private String country;
    @Column(name = "SHIPPING_ADDRESS", nullable = false, length = 100)
    private String shippingAddress;
    @Column(name = "SECOND_ADDRESS")
    private String address2;
    @Column(name = "CITY", nullable = false)
    private String city;
    @Column(name = "STATE")
    private String State;
    @Column(name = "ZIP_CODE", nullable = false)
    private String zip;


}
