package com.webshop.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    public Long id;
    @Column(name = "NAME")
    private String articleName;
    @Column(name = "DATE_ADDED", nullable = false)
    private String dateAdded;
    @Column(name = "CONTENT", nullable = false)
    private String content;


}
