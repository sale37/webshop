package com.webshop.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Discount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "DICSOUNT_PRODUCTS")
    @ElementCollection(targetClass = Product.class)
    private List<Product> productsOnDiscount = new ArrayList<>();

    @OneToMany(mappedBy = "discount", fetch = FetchType.EAGER)
    private Set<Product> products;
}
