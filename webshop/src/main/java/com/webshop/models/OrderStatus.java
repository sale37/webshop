package com.webshop.models;

public enum OrderStatus {

    NEW, REJECTED, WAITING_FOR_PAYMENT, COMPLETED
}
