package com.webshop.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;
    @Column(name = "DATE_OF_MANUFACTURE")
    private String dateOfManufacture;
    @Column(name = "PRICE")
    private double price;
    @Column(name = "SERIAL_NUMBER", unique = true)
    private String productSerialNumber;
    @Column(name = "IN_STOCK")
    private int inStock;
    @ManyToOne
    @JoinColumn(name = "discount_id")
    private Discount discount;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_REVIEW_ID")
    private List<ProductReview> reviews = new ArrayList<>();
    @Column(name = "RATING")
    private Double rating;
}
