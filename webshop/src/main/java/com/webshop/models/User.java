package com.webshop.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;
    @Column(name = "USER_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserType userType;
    @Column(name = "NAME", nullable = false, length = 100)
    private String username;
    @Column(name = "PASSWORD", nullable = false, length = 100, unique = true)
    private String password;
    @Column(name = "DATE_OD_BIRTH", length = 100)
    private String dateOfBirth;
    @Column(name = "DATE_OF_REGISTRATION")
    private String dateOfRegistration;
    @Column(name = "EMAIL", nullable = false, length = 100, unique = true)
    private String email;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinTable(name = "user_addreses",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "address_id")})
    private Set<Address> addresses = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private List<Orders> orders = new ArrayList<>();
}
