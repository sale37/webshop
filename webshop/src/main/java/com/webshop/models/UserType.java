package com.webshop.models;

public enum UserType {

    ADMIN, CUSTOMER
}
