package com.webshop.services;

import com.webshop.models.Address;

import java.util.List;

public interface AddressService {

    Address findAdressById(Long id);

    Address findByShippingAddress(String adrress);

    Address update(Address address);

    List<Address> findAll();

    void delete(Address address);


}
