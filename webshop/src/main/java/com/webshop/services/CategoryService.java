package com.webshop.services;

import com.webshop.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> findAll();

    Category findById(Long id);

    Category addCategory(String name);

    Category addProduct(Long categoryId, Long productId);

    void deleteById(Long id);
}
