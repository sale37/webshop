package com.webshop.services;

import com.webshop.models.CartItem;
import com.webshop.models.User;

public interface MailService {

    void sendMail(User user, CartItem cartItem);
}
