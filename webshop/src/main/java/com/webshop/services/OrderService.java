package com.webshop.services;

import com.webshop.models.Orders;

import java.util.List;

public interface OrderService {

    List<Orders> findAll();

    Orders findById(Long id);

    Orders save(Orders order);

    void submitOrder(Long orderId);
}
