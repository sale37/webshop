package com.webshop.services;

import com.webshop.models.ProductReview;

import java.util.List;

public interface ProductReviewService {

    ProductReview showReview(Long productId, Long reviewId);

    List<ProductReview> showAllReviewsForProduct(Long productId);

    ProductReview addReview(Long productId, ProductReview productReview);
}
