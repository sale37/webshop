package com.webshop.services;

import com.webshop.models.Product;

import java.util.List;

public interface ProductService  {

    Product save(Product product);

    List<Product> findAll();

    Product findById(Long id);

    void deleteById(Long id);
}
