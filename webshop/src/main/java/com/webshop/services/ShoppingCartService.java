package com.webshop.services;

import com.webshop.models.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {

    List<ShoppingCart> findAll();

    ShoppingCart createCart();

    ShoppingCart showCart(Long id);

    ShoppingCart addItem(Long cartid, Long productId);

    void checkout(Long cartId, Long userId);
}
