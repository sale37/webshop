package com.webshop.services;

import com.webshop.models.User;

public interface UserService {

    User save(User user);

    User update(User user);

    User findById(Long id);

    Iterable<User> findAll();

    User findByUsername(String username);

    void delete(User user);

    User updateUserPassword(User user, String oldPassword, String newPassword, String repeatNewPassword);
}
