package com.webshop.services.impl;

import com.webshop.data.AddressRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Address;
import com.webshop.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {


    private final AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Address findAdressById(Long id) {
        Optional<Address> addressFromDatabase = addressRepository.findById(id);

        return addressFromDatabase.orElseThrow(() ->
                new ResourceNotFoundException("Address not found"));
    }

    @Override
    public Address findByShippingAddress(String adrress) {

        Optional<Address> addressOptional = addressRepository.findByShippingAddress(adrress);

        return addressOptional.orElseThrow(() ->
                new ResourceNotFoundException("Address with shipping address: " + adrress + " not found"));
    }

    @Override
    public Address update(Address address) {
        Optional<Address> addressOptional = addressRepository.findById(address.getId());
        if (addressOptional.isPresent()) {
            Address addressFromDatabase = addressOptional.get();
            addressFromDatabase.setCountry(address.getCountry());
            addressFromDatabase.setShippingAddress(address.getShippingAddress());
            addressFromDatabase.setAddress2(address.getAddress2());
            addressFromDatabase.setCity(address.getCity());
            addressFromDatabase.setState(address.getState());
            addressFromDatabase.setZip(address.getZip());
        } else {
            throw new ResourceNotFoundException("Address not found");
        }
        return addressRepository.save(addressOptional.get());
    }

    @Override
    public void delete(Address address) {
        Optional<Address> addressToDelete = addressRepository.findById(address.getId());
        if (addressToDelete.isPresent()){
            addressRepository.delete(address);
        }else {
            throw new ResourceNotFoundException("Address not found");
        }
    }

    @Override
    public List<Address> findAll() {
        List<Address> list = (List<Address>) addressRepository.findAll();
        if (!list.isEmpty()){
            return list;
        }else {
            throw new ResourceNotFoundException("Addresses not found");
        }
    }

}
