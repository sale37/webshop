package com.webshop.services.impl;

import com.webshop.data.CategoryRepository;
import com.webshop.exceptions.BadRequestException;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Category;
import com.webshop.models.Product;
import com.webshop.services.CategoryService;
import com.webshop.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    private final ProductService productService;

    @Override
    public List<Category> findAll() {
        log.info("Searching for all categories");
        List<Category> categories = (List<Category>) categoryRepository.findAll();

        if (!categories.isEmpty()){
            return categories;
        }else{
            throw new ResourceNotFoundException("Categories not found");
        }
    }

    @Override
    public Category addCategory(String name) {
        log.info("Adding category with name: " + name);
        Category category = new Category();
        category.setName(name);
        return categoryRepository.save(category);
    }

    @Override
    public Category findById(Long id) {
        log.info("Searching for category with id: " + id);
        Optional<Category> category = categoryRepository.findById(id);
        return category.orElseThrow(() ->
                new ResourceNotFoundException("Category not found"));
    }

    @Override
    public Category addProduct(Long categoryId, Long productId) {
        log.info("Adding product with id: " + productId + " into the category with id: " + categoryId);
        Optional<Category> category = categoryRepository.findById(categoryId);
        Product product = productService.findById(productId);
        if (category.isPresent()) {
            if (!isProductInCategory(category.get(), productId)) {
                category.get().getProducts().add(product);
                return categoryRepository.save(category.get());
            }else {
                throw new BadRequestException("Category already contains this product");
            }
        }else {
            throw new ResourceNotFoundException("Category not found");
        }
    }

    @Override
    public void deleteById(Long id) {
        log.info("Deleting category with id: " + id);
        categoryRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Category not found"));
        categoryRepository.deleteById(id);
    }

    private boolean isProductInCategory(Category category, Long productId){
       return category.getProducts().stream().anyMatch(product -> productId.equals(product.getId()));
    }
}
