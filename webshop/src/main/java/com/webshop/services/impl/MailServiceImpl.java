package com.webshop.services.impl;

import com.webshop.models.CartItem;
import com.webshop.models.User;
import com.webshop.services.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;

    public void sendMail(User user, CartItem cartItem) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();

        mail.setTo(user.getEmail());
        mail.setFrom("saleilic777@gmail.com");
        mail.setSubject("Order rejected");
        mail.setText("Your order has been rejected because there is not " + cartItem.getProduct().getName() + " in stock");

        javaMailSender.send(mail);
    }
}
