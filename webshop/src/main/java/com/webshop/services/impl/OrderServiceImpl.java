package com.webshop.services.impl;

import com.webshop.data.OrderRepository;
import com.webshop.data.ProductRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.*;
import com.webshop.services.MailService;
import com.webshop.services.OrderService;
import com.webshop.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@EnableScheduling
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    private final UserService userService;

    private final MailService mailService;

    @Override
    public List<Orders> findAll() {
        return (List<Orders>) orderRepository.findAll();
    }

    @Override
    public Orders findById(Long id) {
        return orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order not found"));
    }

    @Override
    public Orders save(Orders order) {
        return orderRepository.save(order);
    }

    @Override
    public void submitOrder(Long orderId) {
        log.info("Submitting order number: " + orderId);
        Orders order = this.findById(orderId);

        order.setOrderStatus(OrderStatus.NEW);
        order.setDateOfOrdering(new Date());
        this.save(order);
    }

    @Scheduled(fixedDelay = 1000)
    public void proccesNewOrder() {
        Optional<Orders> order = this.findAll().stream().filter(orders -> OrderStatus.NEW.equals(orders.getOrderStatus())).findFirst();

        if (order.isPresent()) {
            log.info("Processing order with id: " + order.get().getId());
            User user = findUserForOrder(order.get());
            List<CartItem> cartItems = order.get().getOrderContent();
            for (CartItem cartItem : cartItems) {
                if (cartItem.getQuantity() <= cartItem.getProduct().getInStock()) {
                    log.info("Order with id: " + order.get().getId() + " accepted");
                    reduceAvailableProducts(cartItem.getProduct(), cartItem.getQuantity());
                    order.get().setOrderStatus(OrderStatus.WAITING_FOR_PAYMENT);
                    productRepository.save(cartItem.getProduct());
                    orderRepository.save(order.get());
                } else {
                    log.info("Order with id: " + order.get().getId() + " rejected");
                    order.get().setOrderStatus(OrderStatus.REJECTED);
                    orderRepository.save(order.get());
                    mailService.sendMail(user, cartItem);
                }
            }

        }
    }


    private Product reduceAvailableProducts(Product product, int numberOfOrderedProducts) {
        int newInStock = product.getInStock() - numberOfOrderedProducts;
        product.setInStock(newInStock);
        return product;
    }

    private User findUserForOrder(Orders order) {
        List<User> users = (List<User>) userService.findAll();
        User userFound = new User();

        for (User user : users) {
            List<Orders> orders = user.getOrders();
            if (orders.stream().anyMatch(order1 -> order.getId().equals(order1.getId()))) {
                userFound = user;
            }
        }
        return userFound;
    }
}
