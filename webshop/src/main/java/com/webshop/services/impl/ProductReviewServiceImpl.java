package com.webshop.services.impl;

import com.webshop.data.ProductReviewRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Product;
import com.webshop.models.ProductReview;
import com.webshop.services.ProductReviewService;
import com.webshop.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductReviewServiceImpl implements ProductReviewService {

    private final ProductReviewRepository productReviewRepository;

    private final ProductService productService;

    @Override
    public ProductReview showReview(Long productId, Long reviewId) {
        log.info("Searching review with id: " + reviewId + " for product with id: " + productId);
        Product product = productService.findById(productId);
        return reviewFromProduct(product, reviewId);
    }

    @Override
    public List<ProductReview> showAllReviewsForProduct(Long productId) {
        log.info("Searching all reviews for product with id: " + productId);
        Product product = productService.findById(productId);
        if (!product.getReviews().isEmpty()) {
            return product.getReviews();
        }else{
            throw new ResourceNotFoundException("No reviews for product");
        }
    }

    @Override
    public ProductReview addReview(Long productId, ProductReview review) {
        log.info("Adding review to the product with id: " + productId);
        Product product = productService.findById(productId);

        ProductReview productReview = new ProductReview();
        productReview.setProductRating(review.getProductRating());
        productReview.setComment(review.getComment());

        if (product.getReviews().isEmpty()){
            product.getReviews().add(productReview);
            product.setRating(review.getProductRating());
        }else {
            product.getReviews().add(productReview);
            updateRating(product);
        }
        productReviewRepository.save(productReview);
        productService.save(product);
        return productReview;
    }

    private Product updateRating(Product product){
        int numOfReviews = product.getReviews().size();
        double sum = 0;
        double avrRating;
        List<ProductReview> productReviews = product.getReviews();
        for (ProductReview productReview : productReviews) {
            sum += productReview.getProductRating();
        }
        avrRating = sum / numOfReviews;
        product.setRating(avrRating);
        return product;
    }

    private ProductReview reviewFromProduct(Product product, Long reviewId){
        return product.getReviews().stream().filter(productReview ->
                reviewId.equals(productReview.getId())).findFirst().orElseThrow(() ->
                new ResourceNotFoundException("Review not found"));
    }
}
