package com.webshop.services.impl;

import com.webshop.data.ProductRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Product;
import com.webshop.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;


    @Override
    public List<Product> findAll() {
        log.info("Searching for all products");
        List<Product> list = (List<Product>) productRepository.findAll();
        if (!list.isEmpty()){
            return list;
        }else {
            throw new ResourceNotFoundException("No products found");
        }
    }

    @Override
    public Product findById(Long id) {
        log.info("Searching for product with id: " +id);

        return productRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Product not found"));
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteById(Long id) {
        log.info("Deleting product with id: " + id);
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            productRepository.deleteById(id);
        }else {
            throw new ResourceNotFoundException("Product not found");
        }
    }
}
