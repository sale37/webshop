package com.webshop.services.impl;

import com.webshop.data.CartItemRepository;
import com.webshop.data.ProductRepository;
import com.webshop.data.ShoppingCartRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.*;
import com.webshop.services.OrderService;
import com.webshop.services.ShoppingCartService;
import com.webshop.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
@Slf4j
public class ShoppingCartServiceImpl implements ShoppingCartService {


    private final ShoppingCartRepository shoppingCartRepository;

    private final CartItemRepository cartItemRepository;

    private final ProductRepository productRepository;

    private final OrderService orderService;

    private final UserService userService;

    @Override
    public List<ShoppingCart> findAll() {
        log.info("Searching for all shopping carts");
        List<ShoppingCart> list = (List<ShoppingCart>) shoppingCartRepository.findAll();
        if (!list.isEmpty()) {
            return list;
        }else {
            throw new ResourceNotFoundException("Carts not found");
        }
    }

    @Override
    public ShoppingCart createCart() {
        log.info("Creating new shopping cart");
        ShoppingCart shoppingCart = new ShoppingCart();
        return shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public ShoppingCart showCart(Long id) {
        log.info("Searching for shopping cart with id: " + id);
        Optional<ShoppingCart> shoppingCart = shoppingCartRepository.findById(id);

        return shoppingCart.orElseThrow(() -> new ResourceNotFoundException("Cart with id: " + id + " not found"));
    }


    @Override
    @Transactional
    public ShoppingCart addItem(Long cartId, Long productId) {
        log.info("Adding product with id: " + productId + " into the shopping cart with id: " + cartId);
        Product productInDb = productRepository.findById(productId).orElseThrow(() ->
                new ResourceNotFoundException("Product not found"));
        ShoppingCart shoppingCartInDB = shoppingCartRepository.findById(cartId).orElseThrow(() ->
                new ResourceNotFoundException("Shopping cart not found not found"));

        List<CartItem> cartItems = shoppingCartInDB.getCartItems();

        CartItem newCartItem = new CartItem();
        newCartItem.setProduct(productInDb);

        Optional<CartItem> productCartItem = getCartItemWithProduct(cartItems, productId);

        if (canProductBeOrdered(productInDb, productCartItem.map(CartItem::getQuantity).orElse(0))) {
            if (productCartItem.isPresent()) {
                int quantity = productCartItem.get().getQuantity();
                productCartItem.get().setQuantity(quantity+1);
            } else {
                cartItems.add(newCartItem);
                newCartItem.setQuantity(1);
            }
        } else {
            throw new ResourceNotFoundException("Product not in stock");
        }

        ShoppingCart cartToReturn = shoppingCartRepository.save(shoppingCartInDB);
        cartItemRepository.save(newCartItem);
        return cartToReturn;
    }

    @Override
    @Transactional
    public void checkout(Long cartId, Long userId) {
        log.info("Checkout for shopping cart with id: " + cartId + " for user with id: " + userId);
        ShoppingCart shoppingCart = shoppingCartRepository.findById(cartId).orElseThrow(() ->
                new ResourceNotFoundException("Shopping cart not found"));
        User user = userService.findById(userId);

        if (!shoppingCart.getCartItems().isEmpty()) {
            Orders newOrder = new Orders();
            newOrder.setOrderContent(shoppingCart.getCartItems());
            user.getOrders().add(newOrder);
            orderService.save(newOrder);
        }else {
            throw new ResourceNotFoundException("Shopping cart is empty");
        }
        shoppingCartRepository.deleteById(cartId);
    }

    private Optional<CartItem> getCartItemWithProduct(List<CartItem> cartItems, Long productId) {
        return cartItems.stream().filter(cartItem2 -> isProductInCartItem(cartItem2, productId)).findFirst();
    }

    private boolean canProductBeOrdered(Product productInDb, int orderProduct) {
        return productInDb.getInStock() > orderProduct;
    }


    private boolean isProductInCartItem(CartItem cartItem, Long productId) {
        return productId.equals(cartItem.getProduct().getId());
    }
}
