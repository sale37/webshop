package com.webshop.services.impl;

import com.webshop.data.UserRepository;
import com.webshop.exceptions.BadRequestException;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.User;
import com.webshop.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User save(User user) {
        log.info("Adding user with id: " + user.getId());
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        log.info("Updating user with id: " + user.getId());
        Optional<User> userOptional = userRepository.findById(user.getId());
        if (userOptional.isPresent()) {
            User userToUpdate = userOptional.get();
            if (user.getUserType() != null) {
                userToUpdate.setUserType(user.getUserType());
            }
            if (user.getUsername() != null) {
                userToUpdate.setUsername(user.getUsername());
            }
            if (user.getDateOfBirth() != null) {
                userToUpdate.setDateOfBirth(user.getDateOfBirth());
            }
            if (user.getDateOfRegistration() != null) {
                userToUpdate.setDateOfRegistration(user.getDateOfRegistration());
            }
            if (user.getEmail() != null) {
                userToUpdate.setEmail(user.getEmail());
            }
        } else {
            throw new ResourceNotFoundException();
        }
        return userRepository.save(userOptional.get());
    }

    @Override
    public User findById(Long id) {
        log.info("Searching for user with id: " + id);
        Optional<User> user = userRepository.findById(id);

        return user.orElseThrow(() ->
                new ResourceNotFoundException("User with id: " + id + " not found"));
    }

    @Override
    public List<User> findAll() {
        log.info("Searching for all users");
        List<User> list = (List<User>) userRepository.findAll();
        if (!list.isEmpty()) {
            return list;
        } else {
            throw new ResourceNotFoundException("No users found");
        }
    }

    @Override
    public User findByUsername(String username) {
        log.info("Searching for the user with username: " + username);
        Optional<User> user = userRepository.findByUsername(username);

        return user.orElseThrow(() ->
                new ResourceNotFoundException("User with username: " + username + " not found"));
    }

    @Override
    public void delete(User user) {
        log.info("Deleting user with id: " + user.getId());
        Optional<User> userToDelete = userRepository.findById(user.getId());
        if (userToDelete.isPresent()) {
            userRepository.delete(user);
        } else {
            throw new ResourceNotFoundException("User not found");
        }
    }

    @Override
    public User updateUserPassword(User user, String oldPassword, String newPassword, String repeatNewPassword) {
        Optional<User> userFromDataBase = userRepository.findById(user.getId());
        if (userFromDataBase.isPresent()) {
            if (user.getPassword().equals(oldPassword) && newPassword.equals(repeatNewPassword)) {
                userFromDataBase.get().setPassword(newPassword);
                return userRepository.save(userFromDataBase.get());
            } else {
                throw new BadRequestException("Wrong password");
            }
        } else {
            throw new ResourceNotFoundException("User not found");
        }
    }
}
