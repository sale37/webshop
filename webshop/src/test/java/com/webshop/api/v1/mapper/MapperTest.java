package com.webshop.api.v1.mapper;

import com.webshop.api.v1.dto.AddressDTO;
import com.webshop.api.v1.dto.UserDTO;
import com.webshop.models.Address;
import com.webshop.models.User;
import com.webshop.models.UserType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MapperTest {

    UserMapper userMapper = UserMapper.INSTANCE;

    AddressMapper addressMapper = AddressMapper.INSTANCE;


    @Test
    public void customerToUserDtoTest(){

        User user = new User();
        user.setId(1L);
        user.setUsername("user");
        user.setEmail("user");
        user.setPassword("user");
        user.setUserType(UserType.CUSTOMER);

        UserDTO userDTO = userMapper.userToUserDTO(user);

        assertEquals("user", userDTO.getUsername());
    }

    @Test
    public void addressToAddressDTO(){
        Address address = new Address();
        address.setId(1L);
        address.setCountry("address");
        address.setShippingAddress("address");
        address.setCity("address");
        address.setZip("address");

        AddressDTO addressDTO = addressMapper.addressToAddressDTO(address);

        assertEquals("address", addressDTO.getFirstName());
    }

}