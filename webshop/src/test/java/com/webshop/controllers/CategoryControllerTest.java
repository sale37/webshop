package com.webshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webshop.models.Category;
import com.webshop.models.Product;
import com.webshop.services.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CategoryControllerTest {

    @Mock
    private CategoryService categoryService;

    @InjectMocks
    private CategoryController categoryController;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(categoryController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    Category category;
    Product product1;
    Product product2;

    @Before
    public void setup() throws Exception {
        category = new Category();
        category.setId(1L);
        category.setName("category");

        product1 = new Product();
        product1.setId(1L);
        product1.setName("product1");
        product1.setInStock(5);

        product2 = new Product();
        product2.setId(2L);
        product2.setName("product2");
        product2.setInStock(3);

        category.getProducts().add(product1);
        category.getProducts().add(product2);
    }

    @Test
    public void addCategory() throws Exception {
        when(categoryService.addCategory(anyString())).thenReturn(category);

        mockMvc.perform(post(CategoryController.BASE_URL)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .content(asJsonString("category2")))
                .andExpect(status().isCreated());

        verify(categoryService, times(1)).addCategory(anyString());
    }

    @Test
    public void findAll() throws Exception {
        List<Category> categories = new ArrayList<>();
        categories.add(category);

        when(categoryService.findAll()).thenReturn(categories);

        mockMvc.perform(get(CategoryController.BASE_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)));

        verify(categoryService, times(1)).findAll();
    }

    @Test
    public void findById() throws Exception {
        when(categoryService.findById(anyLong())).thenReturn(category);

        mockMvc.perform(get(CategoryController.BASE_URL + "/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(1));

        verify(categoryService, times(1)).findById(anyLong());
    }

    @Test
    public void addProduct() throws Exception {
        when(categoryService.addProduct(anyLong(), anyLong())).thenReturn(category);

        mockMvc.perform(post(CategoryController.BASE_URL + "/{categoryId}/product/{productId}", 1, 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.products", hasSize(2)));

        verify(categoryService, times(1)).addProduct(anyLong(), anyLong());
    }

    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(delete(CategoryController.BASE_URL + "/{id}", 1))
                .andExpect(status().isOk());

        verify(categoryService, times(1)).deleteById(anyLong());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}