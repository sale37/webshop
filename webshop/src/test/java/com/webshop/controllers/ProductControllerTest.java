package com.webshop.controllers;

import com.webshop.models.Product;
import com.webshop.services.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(productController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    Product product1;
    Product product2;

    @Before
    public void setup() throws Exception {

        product1 = new Product();
        product1.setId(1L);
        product1.setName("product1");
        product1.setInStock(5);

        product2 = new Product();
        product2.setId(2L);
        product2.setName("product2");
        product2.setInStock(3);
    }



    @Test
    public void getAllProducts() throws Exception{

        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);

        when(productService.findAll()).thenReturn(products);

        mockMvc.perform(get(ProductController.BASE_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));

        verify(productService, times(1)).findAll();
    }

    @Test
    public void findById() throws Exception {
        when(productService.findById(anyLong())).thenReturn(product1);

        mockMvc.perform(get(ProductController.BASE_URL + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name").value("product1"));

        verify(productService, times(1)).findById(anyLong());
    }

    @Test
    public void deleteById() throws  Exception {
        mockMvc.perform(delete(ProductController.BASE_URL + "/{id}", 1))
                .andExpect(status().isOk());

        verify(productService, times(1)).deleteById(anyLong());
    }
}