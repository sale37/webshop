package com.webshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webshop.models.ProductReview;
import com.webshop.services.ProductReviewService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductReviewControllerTest {

    @Mock
    private ProductReviewService productReviewService;

    @InjectMocks
    private ProductReviewController productReviewController;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(productReviewController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    ProductReview productReview;

    @Before
    public void setup() throws Exception {
        productReview = new ProductReview();
        productReview.setId(1L);
        productReview.setProductRating(9);
        productReview.setComment("comment");
    }

    @Test
    public void addReview() throws Exception {
        when(productReviewService.addReview(anyLong(), any())).thenReturn(productReview);

        mockMvc.perform(post(ProductReviewController.BASE_URL + "/product/{productId}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(productReview)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.comment", is("comment")));

        verify(productReviewService, times(1)).addReview(anyLong(), any(ProductReview.class));
    }

    @Test
    public void showAllReviewsForProduct() throws Exception {
        List<ProductReview> productReviews = Collections.singletonList(productReview);

        when(productReviewService.showAllReviewsForProduct(anyLong())).thenReturn(productReviews);

        mockMvc.perform(get(ProductReviewController.BASE_URL + "/product/{productId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)));

        verify(productReviewService, times(1)).showAllReviewsForProduct(anyLong());
    }

    @Test
    public void showReview() throws  Exception {
        when(productReviewService.showReview(anyLong(), anyLong())).thenReturn(productReview);

        mockMvc.perform(get(ProductReviewController.BASE_URL + "/product/{productId}/review/{reviewId}", 1, 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.comment", is("comment")));

        verify(productReviewService, times(1)).showReview(anyLong(), anyLong());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}