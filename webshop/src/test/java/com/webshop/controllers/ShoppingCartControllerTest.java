package com.webshop.controllers;

import com.webshop.models.CartItem;
import com.webshop.models.Product;
import com.webshop.models.ShoppingCart;
import com.webshop.services.ShoppingCartService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ShoppingCartControllerTest {

    @Mock
    private ShoppingCartService shoppingCartService;

    @InjectMocks
    private ShoppingCartController shoppingCartController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(shoppingCartController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    ShoppingCart shoppingCart1;
    ShoppingCart shoppingCart2;

    Product product;
    CartItem cartItem;

    @Before
    public void setup() throws Exception {
        shoppingCart1 = new ShoppingCart();
        shoppingCart1.setId(1L);

        shoppingCart2 = new ShoppingCart();
        shoppingCart2.setId(2L);

        product = new Product();
        product.setId(1L);
        product.setName("product");
        product.setInStock(5);

        cartItem = new CartItem();
        cartItem.setId(1L);
        cartItem.setProduct(product);
    }

    @Test
    public void findAll() throws Exception{
        List<ShoppingCart> shoppingCarts = Arrays.asList(shoppingCart1, shoppingCart2);

        when(shoppingCartService.findAll()).thenReturn(shoppingCarts);

        mockMvc.perform(get(ShoppingCartController.BASE_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));

        verify(shoppingCartService, times(1)).findAll();
    }

    @Test
    public void createCart() throws Exception {
        when(shoppingCartService.createCart()).thenReturn(shoppingCart1);

        mockMvc.perform(post(ShoppingCartController.BASE_URL))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));

        verify(shoppingCartService, times(1)).createCart();
    }

    @Test
    public void showCart() throws Exception {
        when(shoppingCartService.showCart(anyLong())).thenReturn(shoppingCart1);

        mockMvc.perform(get(ShoppingCartController.BASE_URL + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));

        verify(shoppingCartService, times(1)).showCart(anyLong());
    }

    @Test
    public void addItem() throws Exception {
        List<CartItem> cartItems = Arrays.asList(cartItem);
        shoppingCart1.setCartItems(cartItems);
        when(shoppingCartService.addItem(anyLong(), anyLong())).thenReturn(shoppingCart1);

        mockMvc.perform(post(ShoppingCartController.BASE_URL + "/{cart_id}/product/{product_id}", 1, 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$.id", is(1)));

        verify(shoppingCartService, times(1)).addItem(anyLong(), anyLong());
    }

    @Test
    public void checkout() throws Exception {

        mockMvc.perform(post(ShoppingCartController.BASE_URL + "/{cart_id}/checkout", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());

        verify(shoppingCartService, times(1)).checkout(anyLong(), anyLong());
    }
}