package com.webshop.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webshop.models.User;
import com.webshop.models.UserType;
import com.webshop.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;


    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    User user;
    User testUser;

    @Before
    public void setUp() {

        user = new User();
        user.setId(1L);
        user.setUsername("user");
        user.setEmail("user");
        user.setPassword("user");
        user.setUserType(UserType.CUSTOMER);

        testUser = new User();
        testUser.setId(2L);
        testUser.setUsername("testUser");
        testUser.setEmail("testUser");
        testUser.setPassword("testUser");
        testUser.setUserType(UserType.CUSTOMER);
    }

    @Test
    public void getAllUsers() throws Exception{
        List<User> users = Arrays.asList(user, testUser);
        when(userService.findAll()).thenReturn(users);

        mockMvc.perform(get(UserController.BASE_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));

        verify(userService, times(1)).findAll();
    }

    @Test
    public void getUserById() throws Exception {
        when(userService.findById(anyLong())).thenReturn(user);

        mockMvc.perform(get(UserController.BASE_URL + "/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("user")));

        verify(userService, times(1)).findById(anyLong());
    }

    @Test
    public void addUser() throws Exception{
        when(userService.save(any())).thenReturn(user);

        mockMvc.perform(post(UserController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(user)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("user")));

        verify(userService, times(1)).save(any(User.class));
    }



    @Test
    public void updateUser() throws Exception{
        when(userService.update(any())).thenReturn(user);

        mockMvc.perform(put(UserController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(user)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("user")));

        verify(userService, times(1)).update(any(User.class));
    }

    @Test
    public void getUserByUsername() throws Exception {
        when(userService.findByUsername(anyString())).thenReturn(user);

        mockMvc.perform(get(UserController.BASE_URL + "/search").param("username", "user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.username", is("user")));

        verify(userService, times(1)).findByUsername(anyString());
    }

    @Test
    public void deleteUser() throws Exception {
        mockMvc.perform(delete(UserController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(user)))
                .andExpect(status().isOk());

        verify(userService, times(1)).delete(any(User.class));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}