package com.webshop.services.impl;

import com.webshop.data.AddressRepository;
import com.webshop.models.Address;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class AddressServiceImplTest {

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private AddressServiceImpl addressService;

    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    Address address;
    Address testAddress;

    @Before
    public void setUp(){
        address = new Address();
        address.setId(1L);
        address.setCountry("address");
        address.setShippingAddress("address");
        address.setCity("address");
        address.setZip("address");

        testAddress = new Address();
        testAddress.setId(2L);
        testAddress.setCountry("testAddress");
        testAddress.setShippingAddress("testAddress");
        testAddress.setCity("testAddress");
        testAddress.setZip("testAddress");
    }

    @Test
    public void findByIdTest() {
        Optional adressFound = Optional.of(address);
        when(addressRepository.findById(anyLong())).thenReturn(adressFound);

        Address addressToReturn = addressService.findAdressById(1L);

        assertNotNull(addressToReturn);
        verify(addressRepository).findById(anyLong());
    }

    @Test(expected = RuntimeException.class)
    public void findByIdShouldThrowExceptionIfNotFound(){
        when(addressRepository.findById(anyLong())).thenReturn(Optional.empty());
        addressService.findAdressById(1L);
    }

    @Test
    public void deleteAddressTest(){
        Optional<Address> addressFound = Optional.of(address);

        when(addressRepository.findById(anyLong())).thenReturn(addressFound);

        addressService.delete(addressFound.get());
        verify(addressRepository).delete(any(Address.class));
    }

    @Test(expected = RuntimeException.class)
    public void deleteAddressShouldThrowExceptionIfNotFound(){
        when(addressRepository.findById(anyLong())).thenReturn(Optional.empty());
        addressService.delete(address);
        verify(addressRepository).findById(anyLong());
    }

    @Test
    public void findByShippingAddressTest(){
        Optional<Address> addressFound = Optional.of(address);
        when(addressRepository.findByShippingAddress(any())).thenReturn(addressFound);

        Address addressToReturn = addressService.findByShippingAddress("address");

        assertEquals("address", addressToReturn.getShippingAddress());
        assertNotNull(addressToReturn);
        verify(addressRepository).findByShippingAddress(any());
    }

    @Test(expected = RuntimeException.class)
    public void findByShippingAddressShouldThrowExceptionIfNotFound(){
        when(addressRepository.findByShippingAddress(any())).thenReturn(Optional.empty());

        addressService.findByShippingAddress("/");
        verify(addressRepository).findByShippingAddress(any());
    }


    @Test
    public void updateAddress(){

        Optional<Address> addressFound = Optional.of(address);
        when(addressRepository.findById(anyLong())).thenReturn(addressFound);
        when(addressRepository.save(any())).thenReturn(testAddress);

        Address addressSaved = addressService.update(addressFound.get());

        assertEquals(Long.valueOf(2), addressSaved.getId());
        verify(addressRepository).save(any(Address.class));
    }

    @Test(expected = RuntimeException.class)
    public void updateAddressShouldThrowExceptionIfNotFound(){

        when(addressRepository.findById(anyLong())).thenReturn(Optional.empty());

        addressService.update(address);

        verify(addressRepository).save(any(Address.class));
    }

    @Test
    public void findAllTest(){
        List<Address> addressList = new ArrayList<>();
        addressList.add(address);
        addressList.add(testAddress);

        when(addressRepository.findAll()).thenReturn(addressList);

        List<Address> listToReturn = addressService.findAll();
        assertNotNull(listToReturn);
        assertEquals(2, listToReturn.size());
        verify(addressRepository).findAll();
    }

    @Test(expected = RuntimeException.class)
    public void findAllTestIfListIsEmpty(){
        List<Address> addressList = new ArrayList<>();

        when(addressRepository.findAll()).thenReturn(addressList);

        addressService.findAll();
    }
}
