package com.webshop.services.impl;

import com.webshop.data.CategoryRepository;
import com.webshop.exceptions.BadRequestException;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Category;
import com.webshop.models.Product;
import com.webshop.services.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class CategoryServiceImplTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private ProductService productService;

    @InjectMocks
    private CategoryServiceImpl categoryService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    Category category1;
    Category category2;

    Product product1;
    Product product2;

    @Before
    public void setup() throws Exception {
        category1 = new Category();
        category1.setId(1L);
        category1.setName("category1");

        category2 = new Category();
        category2.setId(2L);
        category2.setName("category2");

        product1 = new Product();
        product1.setId(1L);
        product1.setName("product");
        product1.setInStock(5);

        product2 = new Product();
        product1.setId(2L);
        product2.setName("product2");
        product2.setInStock(3);
    }

    @Test
    public void findAll() {
        List<Category> categories = new ArrayList<>();
        categories.add(category1);
        categories.add(category2);

        when(categoryRepository.findAll()).thenReturn(categories);

        List<Category> returnedList = categoryService.findAll();

        assertEquals(2, returnedList.size());
        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    public void addCategory(){
        when(categoryRepository.save(any())).thenReturn(category1);

        Category category = categoryService.addCategory(anyString());

        assertEquals(Long.valueOf(1), category.getId());
        assertEquals("category1", category.getName());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findAllEmpty() {
        List<Category> categories = new ArrayList<>();

        when(categoryRepository.findAll()).thenReturn(categories);

        categoryService.findAll();
    }

    @Test
    public void findById() {
        Optional<Category> categoryOptional = Optional.of(category1);

        when(categoryRepository.findById(anyLong())).thenReturn(categoryOptional);

        Category returnedCategory = categoryService.findById(1L);

        assertEquals(Long.valueOf(1), returnedCategory.getId());
        assertEquals("category1", returnedCategory.getName());
        verify(categoryRepository, times(1)).findById(anyLong());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findByIdCateoryNotFound() {

        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());

        categoryService.findById(1L);
    }

    @Test
    public void addProduct() {
        Optional<Category> categoryOptional = Optional.of(category1);

        when(categoryRepository.findById(anyLong())).thenReturn(categoryOptional);
        when(productService.findById(anyLong())).thenReturn(product1);
        when(categoryRepository.save(any())).thenReturn(category1);
        when(productService.save(any())).thenReturn(product1);

        Category returneCategory = categoryService.addProduct(1L, 1L);

        assertEquals(Long.valueOf(1), returneCategory.getId());
    }

    @Test(expected = BadRequestException.class)
    public void addProductThatAlreadyExistsInCategory() {
        Optional<Category> categoryOptional = Optional.of(category1);
        List<Product> products = Collections.singletonList(product1);
        category1.setProducts(products);

        when(categoryRepository.findById(anyLong())).thenReturn(categoryOptional);
        when(productService.findById(anyLong())).thenReturn(product1);

        categoryService.addProduct(categoryOptional.get().getId(), product1.getId());
    }


    @Test(expected = ResourceNotFoundException.class)
    public void addProductNotFound() {

        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());

        categoryService.addProduct(1L, 1L);
    }

    @Test
    public void deleteById() {
        Optional<Category> categoryOptional = Optional.of(category1);

        when(categoryRepository.findById(anyLong())).thenReturn(categoryOptional);

        categoryService.deleteById(1L);
        verify(categoryRepository, times(1)).deleteById(anyLong());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void deleteByIdCategoryNotFound() {

        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());

        categoryService.deleteById(1L);
    }
}