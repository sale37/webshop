package com.webshop.services.impl;

import com.webshop.data.ProductReviewRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Product;
import com.webshop.models.ProductReview;
import com.webshop.services.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class ProductReviewServiceImplTest {

    @Mock
    private ProductReviewRepository productReviewRepository;

    @Mock
    private ProductService productService;


    @InjectMocks
    private ProductReviewServiceImpl productReviewService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    ProductReview productReview1;
    ProductReview productReview2;
    ProductReview productReview3;

    Product product;
    Product product2;

    @Before
    public void setup() throws Exception {
        productReview1 = new ProductReview();
        productReview1.setId(1L);
        productReview1.setComment("comment1");
        productReview1.setProductRating(9);

        productReview2 = new ProductReview();
        productReview2.setId(2L);
        productReview2.setComment("comment2");
        productReview2.setProductRating(8);

        productReview3 = new ProductReview();
        productReview3.setId(3L);
        productReview3.setComment("comment3");
        productReview3.setProductRating(7);

        product = new Product();
        product.setId(1L);
        product.setName("product");
        product.setInStock(5);

        product2 = new Product();
        product2.setId(2L);
        product2.setName("product2");
        product2.setInStock(3);

        List<ProductReview> productReviews = new ArrayList<>();

        productReviews.add(productReview1);
        productReviews.add(productReview2);

        product.setReviews(productReviews);

    }

    @Test
    public void showReview() {
        when(productService.findById(anyLong())).thenReturn(product);

        ProductReview productReview = productReviewService.showReview(product.getId(), productReview1.getId());

        assertEquals("comment1", productReview.getComment());
        assertEquals(9, productReview.getProductRating(), 0);

        verify(productService, times(1)).findById(anyLong());
    }
    @Test(expected = ResourceNotFoundException.class)
    public void showReviewNotFound() {
        when(productService.findById(anyLong())).thenReturn(product2);

        productReviewService.showReview(1L, 2L);
    }


    @Test
    public void showAllReviewsForProduct() {
        when(productService.findById(anyLong())).thenReturn(product);

        List<ProductReview> productReviews = productReviewService.showAllReviewsForProduct(1L);

        assertEquals(2, productReviews.size());

        verify(productService, times(1)).findById(anyLong());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void showAllReviewsForProductNotFound() {
        when(productService.findById(anyLong())).thenReturn(product2);

        productReviewService.showAllReviewsForProduct(3L);
    }

    @Test
    public void addFirstReview() throws Exception {
        when(productService.findById(anyLong())).thenReturn(product2);
        when(productReviewRepository.save(any())).thenReturn(productReview1);
        when(productService.save(any())).thenReturn(product2);

        ProductReview productReview = productReviewService.addReview(product2.getId(), productReview1);

        assertEquals("comment1", productReview.getComment());
        assertEquals(9, productReview.getProductRating(), 0);
    }

    @Test
    public void addReviewIfThereIsAlreadyReviewForProduct() throws Exception {
        when(productService.findById(anyLong())).thenReturn(product);
        when(productReviewRepository.save(any())).thenReturn(productReview3);
        when(productService.save(any())).thenReturn(product);

        ProductReview productReview = productReviewService.addReview(product.getId(), productReview3);

        assertEquals("comment3", productReview.getComment());
        assertEquals(7, productReview.getProductRating(), 0);
    }
}