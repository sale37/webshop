package com.webshop.services.impl;

import com.webshop.data.ProductRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.Product;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    Product product1;
    Product product2;

    @Before
    public void setup() throws Exception {
        product1 = new Product();
        product1.setId(1L);
        product1.setName("product");
        product1.setInStock(5);

        product2 = new Product();
        product1.setId(2L);
        product2.setName("product2");
        product2.setInStock(3);
    }

    @Test
    public void findAll() {
        List<Product> products = Arrays.asList(product1, product2);

        when(productRepository.findAll()).thenReturn(products);

        List<Product> returnedProducts = productService.findAll();

        assertEquals(2, returnedProducts.size());
        verify(productRepository, times(1)).findAll();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findAllWhenListIsEmpty() {
        List<Product> products = new ArrayList<>();

        when(productRepository.findAll()).thenReturn(products);

        List<Product> returnedProducts = productService.findAll();
    }

    @Test
    public void findById() {
        Optional<Product> productOptional = Optional.of(product1);

        when(productRepository.findById(anyLong())).thenReturn(productOptional);

        Product returnedProduct = productService.findById(1L);

        assertEquals("product", returnedProduct.getName());
        verify(productRepository, times(1)).findById(anyLong());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findByIdProductNotFound() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        productService.findById(1L);
    }


    @Test
    public void delete() {
        Optional<Product> productOptional = Optional.of(product1);

        when(productRepository.findById(anyLong())).thenReturn(productOptional);

        productService.deleteById(product1.getId());
        verify(productRepository, times(1)).deleteById(anyLong());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void deleteProductNotFound() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        productService.deleteById(1L);
    }
}