package com.webshop.services.impl;

import com.webshop.data.CartItemRepository;
import com.webshop.data.ProductRepository;
import com.webshop.data.ShoppingCartRepository;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.*;
import com.webshop.services.OrderService;
import com.webshop.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ShoppingCartServiceImplTest {

    @Mock
    private ShoppingCartRepository shoppingCartRepository;
    @Mock
    private CartItemRepository cartItemRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    OrderService orderService;
    @Mock
    UserService userService;

    @InjectMocks
    private ShoppingCartServiceImpl shoppingCartService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    ShoppingCart shoppingCart1;
    ShoppingCart shoppingCart2;

    Product product;
    CartItem cartItem;

    User user;

    Orders order;

    @Before
    public void setup() throws Exception {
        shoppingCart1 = new ShoppingCart();
        shoppingCart1.setId(1L);
        shoppingCart2 = new ShoppingCart();
        shoppingCart2.setId(2L);

        product = new Product();
        product.setId(1L);
        product.setName("product");
        product.setInStock(5);

        cartItem = new CartItem();
        cartItem.setId(1L);
        cartItem.setProduct(product);

        user = new User();
        user.setId(1L);
        user.setUsername("user");
        user.setEmail("user");
        user.setPassword("user");
        user.setDateOfBirth("user");
        user.setDateOfRegistration("user");
        user.setUserType(UserType.CUSTOMER);

        order = new Orders();
        order.setId(1L);
        order.setOrderStatus(OrderStatus.NEW);
    }

    @Test
    public void findAll() {
        List<ShoppingCart> shoppingCarts = Arrays.asList(shoppingCart1, shoppingCart2);

        when(shoppingCartRepository.findAll()).thenReturn(shoppingCarts);

        List<ShoppingCart> returnedList = shoppingCartService.findAll();

        assertEquals(2, returnedList.size());
        verify(shoppingCartRepository, times(1)).findAll();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findAllWhenListIsEmpty() {
        List<ShoppingCart> shoppingCarts = new ArrayList<>();

        when(shoppingCartRepository.findAll()).thenReturn(shoppingCarts);

        List<ShoppingCart> returnedList = shoppingCartService.findAll();
    }

    @Test
    public void createCart() {
        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart1);

        ShoppingCart returnedCart = shoppingCartService.createCart();

        assertEquals(Long.valueOf(1), returnedCart.getId());
    }

    @Test
    public void showCart() {
        Optional<ShoppingCart> shoppingCartOptional = Optional.of(shoppingCart1);
        when(shoppingCartRepository.findById(anyLong())).thenReturn(shoppingCartOptional);

        ShoppingCart returnedCart = shoppingCartService.showCart(1L);

        assertEquals(Long.valueOf(1), returnedCart.getId());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void showCartThatDoesntExist() {

        when(shoppingCartRepository.findById(anyLong())).thenReturn(Optional.empty());

        shoppingCartService.showCart(1L);
    }

    @Test
    public void addItem() {
        Optional<Product> productOptional = Optional.of(product);
        Optional<ShoppingCart> shoppingCartOptional = Optional.of(shoppingCart1);

        when(productRepository.findById(anyLong())).thenReturn(productOptional);
        when(shoppingCartRepository.findById(anyLong())).thenReturn(shoppingCartOptional);
        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart1);
        when(cartItemRepository.save(any())).thenReturn(cartItem);

        ShoppingCart returnedCart = shoppingCartService.addItem(shoppingCartOptional.get().getId(), productOptional.get().getId());

        assertEquals(1, returnedCart.getCartItems().size());
        assertEquals(productOptional.get().getId(), returnedCart.getCartItems().stream().findFirst().get().getProduct().getId());
        verify(shoppingCartRepository, times(1)).save(any(ShoppingCart.class));
        verify(cartItemRepository, times(1)).save(any(CartItem.class));
    }

    @Test
    public void addItemIfItemIsInTheCartAlready() {
        Optional<Product> productOptional = Optional.of(product);
        Optional<ShoppingCart> shoppingCartOptional = Optional.of(shoppingCart1);
        List<CartItem> cartItems = Collections.singletonList(cartItem);
        shoppingCartOptional.get().setCartItems(cartItems);

        when(productRepository.findById(anyLong())).thenReturn(productOptional);
        when(shoppingCartRepository.findById(anyLong())).thenReturn(shoppingCartOptional);
        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart1);
        when(cartItemRepository.save(any())).thenReturn(cartItem);

        shoppingCartService.addItem(shoppingCartOptional.get().getId(), productOptional.get().getId());

        verify(shoppingCartRepository, times(1)).save(any(ShoppingCart.class));
        verify(cartItemRepository, times(1)).save(any(CartItem.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void addItemNoMoreInStockWhenProductIsAlreadyInCart() {
        Optional<Product> productOptional = Optional.of(product);
        Optional<ShoppingCart> shoppingCartOptional = Optional.of(shoppingCart1);
        product.setInStock(0);
        List<CartItem> cartItems = Collections.singletonList(cartItem);
        shoppingCartOptional.get().setCartItems(cartItems);

        when(productRepository.findById(anyLong())).thenReturn(productOptional);
        when(shoppingCartRepository.findById(anyLong())).thenReturn(shoppingCartOptional);
        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart1);
        when(cartItemRepository.save(any())).thenReturn(cartItem);

        shoppingCartService.addItem(shoppingCartOptional.get().getId(), productOptional.get().getId());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void addItemNoMoreInStock() {
        product.setInStock(0);
        Optional<Product> productOptional = Optional.of(product);
        Optional<ShoppingCart> shoppingCartOptional = Optional.of(shoppingCart1);

        when(productRepository.findById(anyLong())).thenReturn(productOptional);
        when(shoppingCartRepository.findById(anyLong())).thenReturn(shoppingCartOptional);
        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart1);
        when(cartItemRepository.save(any())).thenReturn(cartItem);

       shoppingCartService.addItem(shoppingCartOptional.get().getId(), productOptional.get().getId());
    }

    @Test
    public void checkout() throws Exception {
        Optional<ShoppingCart> shoppingCartOptional = Optional.of(shoppingCart1);
        shoppingCart1.getCartItems().add(cartItem);
        order.getOrderContent().add(cartItem);

        when(shoppingCartRepository.findById(anyLong())).thenReturn(shoppingCartOptional);
        when(userService.findById(anyLong())).thenReturn(user);
        when(orderService.save(any())).thenReturn(order);

        shoppingCartService.checkout(1L, 1L);

        assertEquals(1, user.getOrders().size());
        verify(shoppingCartRepository, times(1)).findById(anyLong());
        verify(userService, times(1)).findById(anyLong());
        verify(orderService, times(1)).save(any(Orders.class));
        verify(shoppingCartRepository, times(1)).deleteById(anyLong());
    }
}