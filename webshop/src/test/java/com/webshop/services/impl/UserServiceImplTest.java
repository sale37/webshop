package com.webshop.services.impl;

import com.webshop.data.UserRepository;
import com.webshop.exceptions.BadRequestException;
import com.webshop.exceptions.ResourceNotFoundException;
import com.webshop.models.User;
import com.webshop.models.UserType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;


    User user;
    User testUser;

    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void setUp() {

        user = new User();
        user.setId(1L);
        user.setUsername("user");
        user.setEmail("user");
        user.setPassword("user");
        user.setDateOfBirth("user");
        user.setDateOfRegistration("user");
        user.setUserType(UserType.CUSTOMER);

        testUser = new User();
        testUser.setId(2L);
        testUser.setUsername("testUser");
        testUser.setEmail("testUser");
        testUser.setDateOfBirth("testUser");
        testUser.setDateOfRegistration("testUser");
        testUser.setPassword("testUser");
        testUser.setUserType(UserType.CUSTOMER);
    }


    @Test
    public void findByIdTest(){
        Optional<User> userOptional = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userOptional);

        User userReturned = userService.findById(1L);

        assertNotNull(userReturned);
        assertEquals(Long.valueOf(1), userReturned.getId());
        verify(userRepository, times(1)).findById(anyLong());
    }


    @Test(expected = ResourceNotFoundException.class)
    public void userServiceShouldTrowExceptionIfUserNotExist() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        userService.findById(1L);
    }

    @Test
    public void findAllTest() {

        List<User> userList = new ArrayList<>();
        userList.add(user);
        userList.add(testUser);

        when(userRepository.findAll()).thenReturn(userList);

        List<User> listToReturn = userService.findAll();
        assertNotNull(listToReturn);
        assertEquals(2, listToReturn.size());
        verify(userRepository).findAll();
    }

    @Test(expected = ResourceNotFoundException.class)
    public void findAllTestIfListIsEmpty() {
        List<User> userList = new ArrayList<>();

        when(userRepository.findAll()).thenReturn(userList);

        userService.findAll();
    }

    @Test
    public void saveUserTest() {

        when(userRepository.save(any())).thenReturn(user);

        User userReturned = userService.save(testUser);

        assertNotNull(userReturned);
        verify(userRepository).save(any(User.class));

    }

    @Test
    public void updateUserTest() {
        User updatedUser = new User();
        updatedUser.setId(3L);
        updatedUser.setUsername("updatedUser");

        Optional<User> userOptionalToUpdate = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userOptionalToUpdate);
        when(userRepository.save(any())).thenReturn(updatedUser);

        User userSaved = userService.update(userOptionalToUpdate.get());

        assertEquals(Long.valueOf(3), userSaved.getId());
        verify(userRepository).save(any(User.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void updateUserShoudThrowExceptionIfUserDoesntExists(){
        User updatedUser = new User();
        updatedUser.setId(3L);
        updatedUser.setUsername("updatedUser");

        Optional<User> userOptionalToUpdate = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        User userSaved = userService.update(userOptionalToUpdate.get());

        assertEquals(Long.valueOf(3), userSaved.getId());
        verify(userRepository).save(any(User.class));
    }

    @Test
    public void deleteUserTest() {

        Optional<User> userFound = Optional.of(user);
        when(userRepository.findById(anyLong())).thenReturn(userFound);

        userService.delete(userFound.get());
        verify(userRepository).delete(any(User.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void deleteUserShouldThrowExceptionIfUserDontExists(){

        when(userRepository.findById(any())).thenReturn(Optional.empty());

        userService.delete(user);
        verify(userRepository, never()).delete(user);
    }

    @Test
    public void findByUsernameShouldReturnUser(){

        Optional<User> userFound = Optional.of(user);
        when(userRepository.findByUsername(any())).thenReturn(userFound);

        User userReturned = userService.findByUsername("user");
        assertNotNull(userReturned);
        assertEquals("user", userReturned.getUsername());
    }



    @Test(expected = ResourceNotFoundException.class)
    public void findByUsernameShouldThrowExceptionIfUsernameDoesntExists(){

        when(userRepository.findByUsername(any())).thenReturn(Optional.empty());

        userService.findByUsername("user");
        verify(userRepository).findByUsername(user.getUsername());
    }

    @Test
    public void updateUserPassword(){
        Optional<User> userFound = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userFound);
        when(userRepository.save(user)).thenReturn(user);

        String oldPassword = "user";
        String newPassword = "newUser";

        User userReturned = userService.updateUserPassword(user, oldPassword, newPassword, newPassword);
        assertNotNull(userReturned);
        verify(userRepository).save(any(User.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void updateUserPasswordShouldThrowExceptionIfUserDoesntExists(){
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        String oldPassword = "user";
        String newPassword = "newUser";

        userService.updateUserPassword(user, oldPassword, newPassword, newPassword);
        verify(userRepository, never()).save(any(User.class));

    }

    @Test(expected = BadRequestException.class)
    public void updateUserPasswordShouldThrowExceptionIfOldPasswordIsWrong(){
        Optional<User> userFound = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userFound);

        String oldPassword = "newUser";


        User userReturned = userService.updateUserPassword(user, oldPassword, null, null);
        assertNotNull(userReturned);
        verify(userRepository, never()).save(any(User.class));

    }

    @Test(expected = BadRequestException.class)
    public void updateUserPasswordShouldThrowExceptionIfReapeatedPasswordIsWrong(){
        Optional<User> userFound = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userFound);

        String oldPassword = "user";
        String newPassword = "newUser";
        String repeatPassword = "user";

        User userReturned = userService.updateUserPassword(user, oldPassword, newPassword, repeatPassword);
        assertNotNull(userReturned);
        verify(userRepository, never()).save(any(User.class));
    }




}
